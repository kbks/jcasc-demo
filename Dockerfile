FROM jenkins/jenkins:lts
ARG CURL_OPTIONS=-sSfLk

# Install plugins
COPY plugins.txt /usr/share/jenkins/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/plugins.txt

ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"
COPY config.yml /var/jenkins_home/jenkins.yaml

EXPOSE 8080