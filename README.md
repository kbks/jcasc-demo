# jcasc-demo

This is a short demo for [jcasc](https://github.com/jenkinsci/configuration-as-code-plugin) plugin
Docker image: jenkins lts
Plugins installed in jenkins [pre-install](https://github.com/jenkinsci/docker#preinstalling-plugins) way.
Jenkins basic configuration made with [jcasc](https://github.com/jenkinsci/configuration-as-code-plugin).